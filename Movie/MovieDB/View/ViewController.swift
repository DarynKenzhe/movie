//
//  ViewController.swift
//  MovieDB
//
//  Created by adminIL on 21.06.2024.
//

import UIKit

class ViewController: UIViewController {

    lazy var movieLabel:UILabel = {
       let label = UILabel()
        label.text = "MovieDB"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 36, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    
    lazy var popularBtn: UIButton = {
            let btn = UIButton()
            btn.setTitle("Popular", for: .normal)
        btn.backgroundColor = .gray
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 9
        btn.widthAnchor.constraint(equalToConstant: 89).isActive = true
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        btn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        btn.tag = 0
            return btn
        }()
        
        lazy var nowPlayingBtn: UIButton = {
            let btn = UIButton()
            btn.setTitle("Now Playing", for: .normal)
            btn.backgroundColor = .gray
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.layer.cornerRadius = 9
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            btn.widthAnchor.constraint(equalToConstant: 89).isActive = true
            btn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            btn.tag = 1
            return btn
        }()
        
        lazy var upcomingBtn: UIButton = {
            let btn = UIButton()
            btn.setTitle("Upcoming", for: .normal)
            btn.backgroundColor = .gray
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.layer.cornerRadius = 9
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            btn.widthAnchor.constraint(equalToConstant: 89).isActive = true
            btn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            btn.tag = 2
            return btn
        }()
        
        lazy var topRatedBtn: UIButton = {
            let btn = UIButton()
            btn.setTitle("Top Rated", for: .normal)
            btn.backgroundColor = .gray
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.layer.cornerRadius = 9
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            btn.widthAnchor.constraint(equalToConstant: 89).isActive = true
            btn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            btn.tag = 3
            return btn
        }()
    
    @objc func buttonTapped(_ sender: UIButton) {
        
        let endpoint: MovieEndpoint
        switch sender.tag {
        case 0:
            endpoint = .popular
        case 1:
            endpoint = .nowPlaying
        case 2:
            endpoint =  .upcoming
        case 3:
            endpoint = .topRated
        default:
            return
        }

        updateButtonColors(tag: sender.tag)
        apiRequest(endpoint: endpoint)
    }
    
    func updateButtonColors(tag: Int) {
        let buttons = [popularBtn, nowPlayingBtn, upcomingBtn, topRatedBtn]

        buttons.enumerated().forEach { index, button in
            if index == tag {
                button.backgroundColor = .red
            } else {
                button.backgroundColor = .gray
            }
        }
    }
    
    lazy var movieTableView:UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.dataSource = self
        table.delegate = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(MovieTableViewCell.self, forCellReuseIdentifier: "movie")
        return table
    }()
    
    var movieData:[Result] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        buttonTapped(popularBtn)
    }
    
    func setupUI() {
        view.addSubview(movieLabel)
            view.addSubview(popularBtn)
            view.addSubview(nowPlayingBtn)
            view.addSubview(upcomingBtn)
            view.addSubview(topRatedBtn)
            view.addSubview(movieTableView)
        NSLayoutConstraint.activate([
                movieLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                movieLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                movieLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                popularBtn.topAnchor.constraint(equalTo: movieLabel.bottomAnchor, constant: 10),
                popularBtn.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                popularBtn.widthAnchor.constraint(equalToConstant: 100),
                nowPlayingBtn.topAnchor.constraint(equalTo: movieLabel.bottomAnchor, constant: 10),
                nowPlayingBtn.leadingAnchor.constraint(equalTo: popularBtn.trailingAnchor, constant: 10),
                nowPlayingBtn.widthAnchor.constraint(equalToConstant: 100),
                upcomingBtn.topAnchor.constraint(equalTo: movieLabel.bottomAnchor, constant: 10),
                upcomingBtn.leadingAnchor.constraint(equalTo: nowPlayingBtn.trailingAnchor, constant: 10),
                upcomingBtn.widthAnchor.constraint(equalToConstant: 100),
                topRatedBtn.topAnchor.constraint(equalTo: movieLabel.bottomAnchor, constant: 10),
                topRatedBtn.leadingAnchor.constraint(equalTo: upcomingBtn.trailingAnchor, constant: 10),
                topRatedBtn.widthAnchor.constraint(equalToConstant: 100),
                movieTableView.topAnchor.constraint(equalTo: popularBtn.bottomAnchor, constant: 20),
                movieTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                movieTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                movieTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    func apiRequest(endpoint: MovieEndpoint) {
        NetworkManager.shared.loadMovie(endpoint: endpoint) { result in
            self.movieData = result
            self.movieTableView.reloadData()
        }
    }
    
}
extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        movieData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = movieTableView.dequeueReusableCell(withIdentifier: "movie", for: indexPath) as! MovieTableViewCell
        let movie = movieData[indexPath.row]
        cell.movieTitle.text = movie.title
       guard let url = URL(string: "https://image.tmdb.org/t/p/w500/"+movie.posterPath!)
        else {return cell}
       DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    cell.movieImage.image = UIImage(data: data)
               }
          }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDeteailViewController = MovieDetailViewController()
        let movieID = movieData[indexPath.row].id
        movieDeteailViewController.movieID = movieID ?? 0
        navigationController?.pushViewController(movieDeteailViewController, animated: true)
    }
    
}
